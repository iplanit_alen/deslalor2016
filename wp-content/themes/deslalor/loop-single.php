<?php
/**
 * The loop that displays a single post.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.2
 */
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'blogheader' );
			$url = $thumb['0']; ?>
			<figure class="relative single-featured-image">
				<span class="post-date"><?php echo get_the_date(); ?></span>
				<img src="<?php echo $url; ?>" alt="">
			</figure>
			
			<?php the_content(); ?>
					
			<?php wp_link_pages( array( 'before' => '<nav>' . __( 'Pages:', 'starkers' ), 'after' => '</nav>' ) ); ?>
		
			<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their description, show a bio on their entries  ?>
				<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'starkers_author_bio_avatar_size', 60 ) ); ?>
				<h2><?php printf( esc_attr__( 'About %s', 'starkers' ), get_the_author() ); ?></h2>
				<?php the_author_meta( 'description' ); ?>
					<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
						<?php printf( __( 'View all posts by %s &rarr;', 'starkers' ), get_the_author() ); ?>
					</a>
			<?php endif; ?>
			
			<footer>
				<?php edit_post_link( __( 'Edit', 'starkers' ), '', '' ); ?>
			</footer>
				
		</article>

		<nav class="post-pagination cf">
			<span class="left"><?php previous_post_link( '%link', '' . _x( '&laquo;', 'Previous post link', 'starkers' ) . ' Previous Post' ); ?></span>
			<span class="right"><?php next_post_link( '%link', 'Next Post ' . _x( '&raquo;', 'Next post link', 'starkers' ) . '' ); ?></span>
		</nav>

<?php endwhile; // end of the loop. ?>