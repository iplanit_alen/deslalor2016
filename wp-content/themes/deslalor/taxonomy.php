<?php
/**
 * Template file for displaying Properties for Sale.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */
 
get_header(); ?>

<section class="block-padding white-bg relative">
	
	<div class="outer-wrapper cf">
	 	
	 	<?php get_template_part( 'loop', 'property' ); ?>	 	
	 	
	</div>
	
	<?php include('includes/featured-sold-properties.php'); ?>
	
</section>

<?php get_footer(); ?>