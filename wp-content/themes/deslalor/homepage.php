<?php
/**
 * Template Name: Homepage
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */

get_header(); ?>

<section id="home-intro" class="white-bg home-block intro-block">
	<div class="wrapper intro-block inner-padding">
		<?php the_field('home_intro_content'); ?>
	</div>
		
	<?php include('includes/featured-sold-properties.php'); ?>
		
	<?php include('includes/featured-forsale-properties.php'); ?>

</section>


<?php get_footer(); ?>