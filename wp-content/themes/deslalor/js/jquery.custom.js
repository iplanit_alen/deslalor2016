Number.prototype.formatMoney = function(decPlaces, thouSeparator, decSeparator) {
    var n = this,
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
        decSeparator = decSeparator == undefined ? "." : decSeparator,
        thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
        sign = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

(function($) {
	
	jQuery(document).ready(function($){

	/*  ==========================================================================
		Custom Events
		========================================================================== */
		
		
		$('.menu-home-primary-menu-container').move({
		    breakpoint: 1024,
		    oldLocation: '#site-header .logo-link',
		    newLocation: '#side-navigation .menu',
		    methods: {
		        o: 'insertAfter',
		        n: 'appendTo'
		    }
		});
		
		$('#secondary-navigation').move({
		    breakpoint: 768,
		    oldLocation: '#allcontent',
		    newLocation: '#side-navigation .menu .menu-home-primary-menu-container',
		    methods: {
		        o: 'prependTo',
		        n: 'insertBefore'
		    }
		});
		
		
		
		$('.grid').masonry({
		  	// set itemSelector so .grid-sizer is not used in layout
		  	itemSelector: '.grid-item',
		  	// use element for option
		  	columnWidth: '.grid-sizer'
		});
		

		
		//Add mobile navigation
		$( "#mobile-menu-toggle, #close-toggle" ).click(function() {
			$( "#allcontent, #side-navigation" ).toggleClass( "selected" );
			$( "#mobile-menu-toggle" ).toggleClass( "fa-navicon" );
				
	  	});	

		/* Parallax */
		var parallax_sections = [ '.parallax-banner' ]; //include all areas where you are applying the parallax code
		var scroll_top = $(window).scrollTop(),
			parallax_supports_centering = ( Modernizr.csstransforms3d ) ? '-50%' : 0,
			i = 0,
			faded = false;
		var interval = setInterval(function() {			
			control_parallax( scroll_top, true );	
			i++;
			if ( i >= 2 && !faded ) {
				$.each( parallax_sections, function( key, value ) {
					
					$(value).addClass('faded');
					
				});
				faded = true;
			}
			if ( i >= 10 ) {				
				clearInterval( interval );				
			}			
		}, 250);		
		$(window).scroll(function() {
			var scroll_top = $(window).scrollTop();
			control_parallax( scroll_top, false );	
		});
		function in_viewport( element ) {					
			return ( $(element).visible(true) ) ? true : false;			
		}		
		function control_parallax( scroll_top, initial ) {	
			 		
			$.each( parallax_sections, function( key, value ) {	
			
				if ( $( value ).length ) {
					
					$( value ).each(function() {
						
						var p_section = $(this),
							current_st = scroll_top,
							relative_st = p_section.offset().top - current_st;		
						if ( in_viewport( p_section ) || initial ) {				
							var test = -( relative_st / 2.5 ), //modify the 3.5 to tweak speed
								image = p_section.find('.parallax-image');						
							image.css({
								'-webkit-transform': 'translate3d( ' + parallax_supports_centering +', ' + test + 'px, 0)',
								'transform': 'translate3d( ' + parallax_supports_centering +', ' + test + 'px, 0)'
							});		
						}	
						
					});	
					
				}
				
			});					
		}
		
		//On page smooth scrolling
		$(function() {
		  $('a[href*="#"]:not([href="#"])').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname  && !$(this).hasClass('ui-tabs-anchor')) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html, body').animate({
		          scrollTop: target.offset().top -40
		        }, 1000);
		        return false;
		      }
		    }
		  });
		});
		
		
		//Calculators
		$("#calculate_mortgage").click(function(){
			var amount = parseFloat($("#calculator_item").val());
			var years = parseFloat($("#calculator_item2").val());
			//if(amount > 1000000){ $("#calculator_item3").val(2); }else{ $("#calculator_item3").val(1); }
			var rate = parseFloat($("#calculator_item3").val());
	
			var y = ((rate / 100) / 12);
			var n = years * 12;
	
			//var result = amount * ((Math.pow(y, n) * (y-1)) / Math.pow(y, (n - 1)));
			var result = amount * ((y * Math.pow(1+y, n))/(Math.pow(1+y, n) - 1));
			$("#mortgage_repayment").html(result.formatMoney(2, ',', '.'));
		});
			
		$("#calculate_stamp").click(function(){
			var price = parseFloat($("#calculator_item4").val());
			
			if ((price-1000000) >= 0) {
				var result = ((price-1000000)*0.02) + 10000;
			}else{
				var result = price * 0.01;
			}
	
			$("#stamp_duty").html(result.formatMoney(2, ',', '.'));
		});
		
		$('#terms-property-status .term-item:eq(1) input[type="checkbox"]').prop('checked', 'true');
		
		
		$('.taxonomy-drilldown-checkboxes > div > h4').addClass('drilldown');
		
		$('.taxonomy-drilldown-checkboxes > div').click(function(event) { 
			if ($(event.target).hasClass('drilldown') && $(this).find('ul').hasClass('open')) {
				$('.taxonomy-drilldown-checkboxes > div ul').removeClass('open');
			} else {
				$('.taxonomy-drilldown-checkboxes > div ul').removeClass('open');
				$(this).find('ul').toggleClass('open');	
			}
		});
		
		
		$(document).on('click', function(event) {
		  if (!$(event.target).hasClass('drilldown')) {
		    $('.taxonomy-drilldown-checkboxes > div ul').removeClass('open');
		  }
		});
		
		$(document).on('click', '.taxonomy-drilldown-checkboxes > div ul, .taxonomy-drilldown-checkboxes > div ul li', function(event) {
			event.stopPropagation();
		});
	
		
	/*  ==========================================================================
		jQuery Plugins
		========================================================================== */		
		
		//Slickslider
		$('.slickslider').slick({
			autoplay : true,
	    	autoplaySpeed : 4000,
	    	pauseOnHover : false,
	    	fade : true,
	    	dots : true,
	    	adaptiveHeight : false,
	    	prevArrow : '<i class="slick-prev fa fa-angle-left"></i>',
		    nextArrow : '<i class="slick-next fa fa-angle-right"></i>'
		});
		
		$('.slickcarousel').slick({
		  	infinite: true,
		  	slidesToShow: 3,
		  	slidesToScroll: 3,
		  	prevArrow : '<i class="slick-prev fa fa-angle-left"></i>',
		    nextArrow : '<i class="slick-next fa fa-angle-right"></i>',
		    responsive: [
		    {
		      breakpoint: 1025,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2,
		      }
		    },
		    {
		      breakpoint: 481,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		      }
		    }
		    ]
		});
		
		$('#testimonial-slider').slick({
			autoplay : true,
	    	autoplaySpeed : 10000,
	    	pauseOnHover : false,
	    	fade : false,
	    	dots : true,
	    	adaptiveHeight : true
		});
		
		
		$('.property-slider').slick({
		  	slidesToShow: 1,
		  	slidesToScroll: 1,
		  	arrows: false,
		  	fade: true,
		  	asNavFor: '.property-slider-nav',
		  	prevArrow : '<i class="slick-prev fa fa-angle-left"></i>',
		    nextArrow : '<i class="slick-next fa fa-angle-right"></i>'
		});
		$('.property-slider-nav').slick({
		  	slidesToShow: 4,
		  	slidesToScroll: 1,
		  	asNavFor: '.property-slider',
		  	dots: true,
		  	centerMode: true,
		  	focusOnSelect: true,
		  	prevArrow : '<i class="slick-prev fa fa-angle-left"></i>',
		    nextArrow : '<i class="slick-next fa fa-angle-right"></i>',
		    responsive: [
		    {
		      breakpoint: 481,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1,
		      }
		    }
		    ]
		});
		
		
		/* matchheight.js */
		$('.matchheight').matchHeight();
		
		$('.matchHeight').matchHeight(false);
		
		
		var map = null;	
		$('.acf-map').each(function(){		
			// create map
			map = new_map( $(this) );		
		});		
				
		/* jQuery UI Tabs */
		$( "#tabs" ).tabs({
			activate: function(event, ui) {
			    google.maps.event.trigger(map, 'resize');
			    //center the map
				center_map( map );
			}
		});
				
		/* Fancybox */
		$(".fancybox").fancybox({
			padding : 0
		});				
		$('.fancybox-media').fancybox({
			padding : 0,
			helpers : {
				media : {}
			}
		});
		$(".fancybox-media").fancybox({
			padding : 0,
			width       : 1920,
		    height      : 1080,
		    aspectRatio : true,
		    scrolling   : 'no',
		    helpers : {
				media : {}
			}
		}); 
		
		
		
		
	});
		
	$(window).load(function() {				
		
		//Custom height of home slider
		var viewportHeight = $(window).height(),	
			headerHeight = $('#site-header').outerHeight(),
			alertHeight = $('#alertbar').outerHeight(),
			browserHeight = viewportHeight - headerHeight;
			jQuery( ".browser-height" ).css( "height", browserHeight);
			jQuery( "#site-footer" ).css( "margin-bottom", alertHeight);

			jQuery(window).resize(function(){
				var viewportHeight = $(window).height(),	
				headerHeight = $('#site-header').outerHeight(),
				alertHeight = $('#alertbar').outerHeight(),
				browserHeight = viewportHeight - headerHeight;
				jQuery( ".browser-height" ).css( "height", browserHeight);
				jQuery( "#site-footer" ).css( "margin-bottom", alertHeight);
			});
		
		var bottom = $('#site-header').offset().top,
			header = $('#site-header').outerHeight(),
			secondarynav = $('#secondary-navigation').outerHeight(),
			combinedheader = header + secondarynav;
		
			$(window).scroll(function(){    
			    if ($(this).scrollTop() > bottom){ 
			        $('#site-header').addClass('fixed'); 
			        $('.home-block.intro-block').css({'padding-top': header });
			        $('#page-header').css({'padding-top': combinedheader });
			    }
			    else{
			        $('#site-header').removeClass('fixed');
			        $('.home-block.intro-block').css({'padding-top': '0' });
			        $('#page-header').css({'padding-top': '30px' });
			    }
			    if ($(this).scrollTop() > 120 ){ 
			        $('#site-header').addClass('reduced'); 
			    }
			    else {
			        $('#site-header').removeClass('reduced');
			    }
			});
			
			
			
			var propertySearchFilterForm = $('.taxonomy-drilldown-checkboxes');
			
			if (propertySearchFilterForm.length) {
				propertySearchFilterForm.on('submit', function(event) {
					if (!$(this).serializeArray().length) {
						event.preventDefault();
						window.location = window.location.origin + '/property-status/for-sale';
					}
				});
			}
		
	});
	
	
	//Fitvid.js
	$("article").fitVids();
	
	
	$('p > img').unwrap(); 
	
	
	
	

})(jQuery);
