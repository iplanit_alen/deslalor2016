<?php $posts = get_field('featured_sold_properties', 'options'); ?>
	<div class="slickcarousel-header">
		<a class="button lightblue property-button" href="<?php echo home_url( '/' ); ?>property-status/sold" title="">View more sold properties &gt;</a>
		<h2 class="sold-title icon-title">Properties We’ve Sold</h2>
	</div>
	<div class="slickcarousel">
	<?php if( $posts ): ?>
		<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
			<?php setup_postdata($post); ?>
		    <div id="post-<?php the_ID(); ?>" <?php post_class('relative'); ?>>
		    	<figure class="relative">
		    		<div class="property-label">
			    		<span class="left">Sold</span>
		    		</div>
			    	<a href="<?php the_permalink(); ?>" title="">
				    	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'property-thumb' );
				    	$url = $thumb['0']; ?>
				    	<img src="<?php echo $url; ?>" alt="" />
			    	</a>
			    </figure>
		    	<div class="navy-bg property-preview matchheight cf">
			    	<h3><?php the_title(); ?></h3><br />
			    	<span class="subtitle"><?php the_field('property_subtitle'); ?></span>
		    	</div>
		    	<?php if(get_field('sold_listing_-_hover_text')) { ?>
			    	<div class="fadeover overlay navy-bg">
				    	<div class="slide-overlay-inner centered">
					    	
					    	<p><?php the_field('sold_listing_-_hover_text'); ?></p>
					    	<a class="read-more uppercase" href="<?php the_permalink(); ?>" title="">View full story</a>
				    	</div>
			    	</div>
		    	<?php } ?>
		    </div>
		<?php endforeach; ?>
		</div>
	<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>