<?php $posts = get_field('featured_for_sale_properties', 'options'); ?>
	<div class="selling-title-wrap relative">
		<a class="button lightblue property-button" href="<?php echo home_url( '/' ); ?>property-status/for-sale" title="">View all properties for sale &gt;</a>
		<h2 class="selling-title icon-title">Properties We’re currently selling</h2>
	</div>
	<div class="slickcarousel">
	<?php if( $posts ): ?>
		<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
			<?php setup_postdata($post); ?>
		    <div id="post-<?php the_ID(); ?>" <?php post_class('relative'); ?>>
			    <figure class="relative">
			    	<div class="property-label">
			    		<span class="left">For Sale</span>
			    		<?php if(get_field('property_video')) { ?><a class="left fancybox-media" href="<?php the_field('property_video'); ?>" title=""><i class="fa fa-play-circle-o"></i></a><?php } ?>
			    	</div>
			    	<a href="<?php the_permalink(); ?>" title="">
				    	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'property-thumb' );
				    	$url = $thumb['0']; ?>
				    	<img src="<?php echo $url; ?>" alt="" />
			    	</a>
			    </figure>
		    	<div class="navy-bg property-preview matchheight">
			    	<h3><a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a></h3>
			    	<span class="subtitle"><?php the_field('property_subtitle'); ?></span>
			    	<span class="price"><?php the_field('guide_price'); ?></span>
		    	</div>
		    </div>
		<?php endforeach; ?>
	</div>
	<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>