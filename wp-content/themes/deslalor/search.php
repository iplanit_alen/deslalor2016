<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */

get_header(); ?>

<section id="blog-wrapper" class="block-padding white-bg relative">
	<div class="outer-wrapper cf">
	 	<div class="width-34 left">
	    	<?php if ( have_posts() ) : ?>
				<?php get_template_part( 'loop', 'search' ); ?>
			<?php else : ?>
				<h2><?php _e( 'Nothing Found', 'starkers' ); ?></h2>
				<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'starkers' ); ?></p>
			<?php endif; ?>
	 	</div>
		<?php get_sidebar(); ?>
	</div>
</section>

<?php get_footer(); ?>