<?php
/**
 * The loop that displays properties for sale.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */
 global $wp_query;
?>

<?php include('includes/filter-bar.php'); ?>

<div class="property-list cf">
		 	
<?php if(function_exists('wp_paginate')) {
    wp_paginate();
} ?>

<span class="property-count left">Total Results (<?php echo $wp_query->found_posts; ?>)</span>

<?php
	$query_filter = $_GET;
	
	if ($query_filter && isset($query_filter['qmt'])) {
		$filters = $query_filter['qmt'];
		$filters_formatted = array();
		
		foreach ($filters as $filter) {
			if (count($filter) > 0) {
				foreach ($filter as $f) {
					$f = get_term($f);
					$taxonomy = $f->taxonomy;
					
					if (!isset($filters_formatted[$taxonomy])) {
						$filters_formatted[$taxonomy] = array();
					}
					
					$filters_formatted[$taxonomy][] = $f->name;
				}
			} else {
				$term = get_term($filter[0]);
				
				if (is_a($term, 'WP_Term')) {
					$filters_formatted[$term->taxonomy] = $term->name;
				}
			}
		}
		
		if (!empty($filters_formatted)) {
			echo '<ul class="current-filters">';
			
			foreach ($filters_formatted as $name => $value) {
				if (!is_array($value)) {
					$value = array($value);
				}
				
				printf('<li><strong>%s</strong>: %s</li>', ucwords(str_replace('-', ' ', $name)), implode(', ', $value));
			}
			
			echo '</ul>';
		}
	}
?>

<div class="clear"></div>
 
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class('relative cf'); ?>>
	 		<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'sliderthumb' );
	 		$url = $thumb['0']; ?> 
	 		<figure class="property-thumb left relative">
	 			<span class="guide-price"><?php the_field('guide_price'); ?></span>
	 			<a href="<?php the_permalink(); ?>" title="">
	    			<img src="<?php echo $url; ?>" alt="" />
	 			</a>
	 		</figure>
	 		<div class="property-preview-content col23 right">
	    		<header>
	   	 			<h1><a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a></h1>
	   	 			<span class="subtitle"><?php the_field('property_subtitle'); ?></span>
	   	 			<span class="subtitle"><?php the_field('property_size'); ?></span>
	    		</header>
	    		<?php the_excerpt(); ?>
	    		<a class="button left" href="<?php the_permalink(); ?>" title="">View property</a>
	    		<?php if(get_field('property_video')) { ?>
	    			<a class="video-link fancybox-media" href="<?php the_field('property_video'); ?>" title="">Watch Video</a>
	    		<?php } ?>
	 		</div>
	   </article>
<?php endwhile; ?>

<?php if(function_exists('wp_paginate')) {
    wp_paginate();
} ?>

<?php endif; ?>

</div>