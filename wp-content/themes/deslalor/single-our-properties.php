<?php
/**
 * Template file for displaying Single Sold Properties.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */
 
get_header(); ?>


<?php if( has_term( 'sold', 'property-status' ) ) { ?>

	<div class="property-intro lightblue-bg">
		<div class="outer-wrapper cf">
			<div class="width-60 left property-wrapper">
				<?php $images = get_field('slideshow_images');
				
				if( $images ): ?>
				    <div class="property-slider">
				    	<?php foreach( $images as $image ): ?>
				    	    <div>
					    	    <a class="fancybox" href="<?php echo $image['sizes']['large']; ?>" rel="gal" title="">
					    	        <img src="<?php echo $image['sizes']['property-thumb']; ?>" alt="<?php echo $image['alt']; ?>" />
					    	    </a>
				    	    </div>
				    	<?php endforeach; ?>
				    </div>
				    <div class="property-slider-nav">
				        <?php foreach( $images as $image ): ?>
				            <div>
				                <img src="<?php echo $image['sizes']['sliderthumb']; ?>" alt="<?php echo $image['alt']; ?>" />
				            </div>
				        <?php endforeach; ?>
				    </div>
				<?php endif; ?>
			</div>	
			<div class="col3 right">
				<?php the_field('property_intro_text'); ?>
				<div class="property-contact-box">
					<span class="red-bg">Contact <strong><?php the_field('contact_name'); ?></strong> to sell your home</span>
					<div class="white-bg inner-padding">
						<ul class="">
							<li><i class="fa fa-phone"></i> <a href="tel:<?php the_field('contact_phone_number'); ?>" title=""><?php the_field('contact_phone_number'); ?></a></li>
							<li><i class="fa fa-envelope"></i> <a href="mailto:<?php the_field('contact_email_address'); ?>" title=""><?php the_field('contact_email_address'); ?></a></li>
						</ul>
					</div>
				</div>
			</div>
					
		</div>
	</div>
	
	<?php
	$post_object = get_field('related_testimonial');
	
	if( $post_object ): 
	
		// override $post
		$post = $post_object;
		setup_postdata( $post ); 
	
		?>
	    <div id="testimonials-wrapper" class="light-grey-bg light-blue-notch large-padding">
			<div class="wrapper centered">
				<h3>Testimonial</h3>	
				<div class="testimonials-inner">
					<?php the_content(); ?>
					<div class="testimonial-author"><?php the_title(); ?> - <span class="red-highlight"><?php the_field('company'); ?></span></div>
				</div>
				<a class="button" href="<?php echo home_url( '/' ); ?>testimonials/" title="View testimonials">View testimonials</a>	
			</div>
	    </div>
	    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	<?php endif; ?>
	
	
	<?php if(get_field('property_summary')) { ?>
		<article class="lightblue-bg large-padding">
			<div class="outer-wrapper two-columns">
				<h3>Summary</h3>
				<?php the_field('property_summary'); ?>
			</div>
		</article>
	<?php } ?>
	

<?php } else { ?>

	<div class="property-intro lightblue-bg">
		<div class="outer-wrapper cf">
			<div class="width-60 left property-wrapper">
				<?php $images = get_field('slideshow_images');
				
				if( $images ): ?>
				    <div class="property-slider">
				    	<?php foreach( $images as $image ): ?>
				    	    <div>
					    	    <a class="fancybox" href="<?php echo $image['sizes']['large']; ?>" rel="gal" title="">
					    	        <img src="<?php echo $image['sizes']['property-thumb']; ?>" alt="<?php echo $image['alt']; ?>" />
					    	    </a>
				    	    </div>
				    	<?php endforeach; ?>
				    </div>
				    <div class="property-slider-nav">
				        <?php foreach( $images as $image ): ?>
				            <div>
				                <img src="<?php echo $image['sizes']['sliderthumb']; ?>" alt="<?php echo $image['alt']; ?>" />
				            </div>
				        <?php endforeach; ?>
				    </div>
				<?php endif; ?>
			</div>
			<div class="col3 right">
				<?php the_field('property_intro_text'); ?>
				<span class="guide-price">Guide Price <strong><?php the_field('guide_price'); ?></strong></span>
				<?php if(get_field('property_video')) { ?>
					<a class="property-video fancybox-media" href="<?php the_field('property_video'); ?>" title="">
						<i class="fa fa-play-circle-o"></i> Watch Video
					</a>
				<?php } ?>
				<div class="property-contact-box">
					<div class="white-bg inner-padding">
						<span class="contact-name">Contact <strong><?php the_field('contact_name'); ?></strong></span>
						<ul class="">
							<li><i class="fa fa-phone"></i> <a href="tel:<?php the_field('contact_phone_number'); ?>" title=""><?php the_field('contact_phone_number'); ?></a></li>
							<li><i class="fa fa-envelope"></i> <a href="mailto:<?php the_field('contact_email_address'); ?>" title=""><?php the_field('contact_email_address'); ?></a></li>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	
	<div class="forsale-property-content white-bg">
		<div class="outer-wrapper">
			
			<div id="tabs">
				
				<ul>
					<?php if(get_field('overview_content')) { ?>
						<li><a href="#overview" title="Overview">Overview</a></li>
					<?php } ?>
					<?php if(get_field('map_location')) { ?>
						<li><a href="#viewmap" title="View Map">View Map</a></li>
					<?php } ?>
					<?php if(get_field('floor_plans')) { ?>
						<li><a href="#floorplans" title="Floor Plans">Floor Plans</a></li>
					<?php } ?>
				</ul>
			
				<?php if(get_field('overview_content')) { ?>
					<div id="overview" class="cf">
						<article class="width-60 left">
							<?php the_field('overview_content'); ?>
						</article>
						<aside class="col3 right">
							<div class="sidebar-block">
								<h4>BER Details</h4>
								<?php if(get_field('ber')) { ?>
									<span>BER: <strong><?php the_field('ber'); ?></strong></span>
								<?php } ?>
								<?php if(get_field('ber_no')) { ?>
									<span>BER No: <strong><?php the_field('ber_no'); ?></strong></span>
								<?php } ?>
								<?php if(get_field('performance_indicator')) { ?>
									<span>Performance indicator: <strong><?php the_field('performance_indicator'); ?></strong></span>
								<?php } ?>
							</div>
							<?php if(get_field('internal_features')) { ?>
								<div class="sidebar-block">
									<h4>Internal Features</h4>
									<?php the_field('internal_features'); ?>
								</div>
							<?php } ?>
							<?php if(get_field('external_features')) { ?>
								<div class="sidebar-block">
									<h4>External Features</h4>
									<?php the_field('external_features'); ?>
								</div>
							<?php } ?>	
							
							<div id="mortgage-calc" class="sidebar-block calculator-block lightblue-bg inner-padding">
								<h4>Mortgage repayment calculator</h4>
								<p class="calc-intro-text">To get an indication of likely interest rates please check with your financial advisor.</p>
								<ul class="clear">
									<li><label for="calculator_item">Borrowing Amount(€):</label><input type="text" id="calculator_item" /></li>
									<li><label for="calculator_item2">Years:</label><input type="text" id="calculator_item2" /></li>
									<li><label for="calculator_item3">Interest Rate(%):</label><input type="text" id="calculator_item3" /></li>
									<li><label>&nbsp;</label><button class="button button_red" type="button" id="calculate_mortgage">Calculate</button></li>
								</ul>
								<p>Monthly Repayments:</p>
								<p><strong>€<span id="mortgage_repayment"></span></strong></p>
							</div>
							
							<div id="stampduty-calc" class="sidebar-block calculator-block lightblue-bg inner-padding">
								<h4>Stamp duty calculator</h4>
								<ul class="clear">
									<li><label for="calculator_item4">Property Value(€):</label><input type="text" id="calculator_item4" /></li>
									<li><label>&nbsp;</label><button class="button" type="button" id="calculate_stamp">Calculate</button></li>
								</ul>
								<p>Stamp Duty:</p>
								<p><strong>€<span id="stamp_duty"></span></strong></p>
							</div>
							
							<!-- Go to www.addthis.com/dashboard to customize your tools -->
							<div class="addthis_sharing_toolbox"></div>
							
						</aside>
					</div>
				<?php } ?>	
				
				<?php if(get_field('map_location')) { ?>
					<div id="viewmap">
						<?php 
						$location = get_field('map_location');
						
						if( !empty($location) ):
						?>
						<div class="acf-map">
							<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
						</div>
						<?php endif; ?>
					</div>
				<?php } ?>
				
				<?php if(get_field('floor_plans')) { ?>
					<div id="floorplans">
						<img src="<?php the_field('floor_plans'); ?>" alt="" />
					</div>
				<?php } ?>
			
			</div>
			
			<a class="back-link" href="<?php echo home_url( '/' ); ?>property-status/for-sale/" title="">&laquo;&nbsp;&nbsp;Back to all properties</a>
			
		</div>
	</div>
	

<?php } ?>  


<?php get_footer(); ?>