<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */

get_header(); ?>

<section id="blog-wrapper" class="block-padding white-bg relative">
	<div class="outer-wrapper cf">
	 	<div class="width-34 left">
	 		<?php get_template_part( 'loop', 'single' ); ?>
	 	</div>
		<?php get_sidebar(); ?>
	</div>
</section>

<?php get_footer(); ?>