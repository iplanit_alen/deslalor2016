<?php
/**
 * The template for displaying Archive pages.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */

get_header(); ?>

<section id="blog-wrapper" class="block-padding white-bg relative">
	<div class="outer-wrapper cf">
		<div class="grid cf">
			<div class="grid-sizer"></div>
			<?php $count=0; ?>
		    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); $count++; ?>
		    	<article class="testimonial-col grid-item">
					<h3><strong><?php the_title(); ?></strong>, <?php the_field('company'); ?></h3>
					<?php
					$post_object = get_field('related_property');
					if( $post_object ): 				
						// override $post
						$post = $post_object;
						setup_postdata( $post ); 
						?>
						<div id="post-<?php the_ID(); ?>" <?php post_class('relative cf'); ?>>
				 			<figure class="relative">
					    		<div class="property-label">
						    		<span class="left">Sold</span>
					    		</div>
						    	<a href="<?php the_permalink(); ?>" title="">
							    	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'property-thumb' );
							    	$url = $thumb['0']; ?>
							    	<img src="<?php echo $url; ?>" alt="" />
						    	</a>
						    </figure>
					    	<div class="navy-bg property-preview matchheight cf">
						    	<h3><?php the_title(); ?></h3><br />
						    	<span class="subtitle"><?php the_field('property_subtitle'); ?></span>
					    	</div>
					    	<?php if(get_field('sold_listing_-_hover_text')) { ?>
						    	<div class="fadeover overlay navy-bg">
							    	<div class="slide-overlay-inner centered">
								    	<img class="icon" src="<?php bloginfo('template_directory'); ?>/images/sold-text-icon.jpg" alt="" />
								    	<p><?php the_field('sold_listing_-_hover_text'); ?></p>
								    	<a class="read-more uppercase" href="<?php the_permalink(); ?>" title="">View full story</a>
							    	</div>
						    	</div>
					    	<?php } ?>
						</div>
					    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					<?php endif; ?>
					<div class="testimonial-content">
						<?php the_content(); ?>
					</div>
		    	</article>
		    <?php endwhile; ?>
		</div>
		
		    <?php if(function_exists('wp_paginate')) {
			    wp_paginate();
			} ?>
	    
	    <?php endif; ?>
	</div>
</section>

<?php get_footer(); ?>