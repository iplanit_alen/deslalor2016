<?php
/**
 * Template Name: About page
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */

get_header(); ?>

<section id="about-intro" class="white-bg block-padding">
	<div class="outer-wrapper">
		<?php get_template_part( 'loop', 'page' ); ?>
	</div>
</section>

<section class="lightblue-bg block-padding relative">
	<div class="outer-wrapper centered">
		<img class="heading-icon" src="<?php bloginfo('template_directory'); ?>/images/why-diff-icon.png" alt="" />
			<div class="thin-intro-text">
				<?php the_field('why_were_different_-_intro_content'); ?>
			</div>
		
		<?php if(get_field('why_were_different_-_columns')): ?>
			<ul class="column-wrapper cf">
				<?php while(has_sub_field('why_were_different_-_columns')): ?>
				<li class="centered col3 left">
					<div class="bordered-circle" style="border-color: <?php the_sub_field('column_title_border_colour'); ?>">
						<h3><?php the_sub_field('column_title'); ?></h3>
					</div>
					<?php the_sub_field('column_text'); ?>
				</li>
				<?php endwhile; ?>	
			</ul>	
		<?php endif; ?>
		
	</div>
</section>	

<section class="white-bg block-padding relative">
	<div class="outer-wrapper">
		<div class="thin-intro-text centered">
			<?php the_field('our_team_-_intro_content'); ?>
		</div>
		
		<?php $the_query = new WP_Query( 'post_type=team-member&posts_per_page=-1&order=ASC&orderby=menu_order' ); $count=0; ?>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); $count++; ?>
				<div class="single-team-member navy-bg cf">
						<?php 
						$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'teamprofile' );
						$url = $thumb['0']; ?>
						<figure class="profile-image left">
							<img src="<?php echo $url; ?>" alt="" />
						</figure>
						<div class="profile-content left">
							<?php the_content(); ?>
						</div>
						
						<div class="profile-contact right">
							<ul class="contact-details">
								<?php if(get_field('landline_phone_number')) { ?>
									<li><a href="tel:<?php the_field('landline_phone_number'); ?>" title=""><i class="fa fa-mobile"></i> <?php the_field('landline_phone_number'); ?></a></li>
								<?php } ?>
								<?php if(get_field('mobile_phone_number')) { ?>
									<li><a href="tel:<?php the_field('mobile_phone_number'); ?>" title=""><i class="fa fa-phone"></i> <?php the_field('mobile_phone_number'); ?></a></li>
								<?php } ?>
								<?php if(get_field('email_address')) { ?>
									<li><a href="mailto:<?php the_field('email_address'); ?>" title=""><i class="fa fa-envelope"></i> <?php the_field('email_address'); ?></a></li>
								<?php } ?>
								<?php if(get_field('linkedin_page_link')) { ?>
									<li><a href="<?php the_field('linkedin_page_link'); ?>" target="_blank" title=""><i class="fa fa-linkedin"></i> Profile</a></li>
								<?php } ?>
							</ul>
						</div>
				</div>
			<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
				
	</div>
</section>




<?php get_footer(); ?>