<?php
/**
 * The loop that displays posts.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */
?>
 
<?php /* If there are no posts to display, such as an empty archive page */ ?>
<?php if ( ! have_posts() ) : ?>
    <h1><?php _e( 'Not Found', 'starkers' ); ?></h1>
    <p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'starkers' ); ?></p>
    <?php get_search_form(); ?>
<?php endif; ?>
 
<?php while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?>>
		
		<?php 
			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
			$url = $thumb['0']; ?>
		
		<figure class="post-thumbnail col3 left">
			<a href="<?php the_permalink(); ?>" title="">
				<img src="<?php echo $url; ?>" alt="" />
			</a>
		</figure>
		
		<div class="post-content col23 right">
		 	<header>
		        <h2><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'starkers' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				<span class="post-date"><?php echo get_the_date(); ?></span>
		 	</header>
		
		 	<?php the_excerpt(); ?>
		 	
		 	<?php if(get_field('custom_read_more_text')) { ?>
		 		<a class="read-more" href="<?php the_permalink(); ?>" title="<?php the_field('custom_read_more_text'); ?>"><?php the_field('custom_read_more_text'); ?> &raquo;</a>
		 	<?php } else { ?>
		 		<a class="read-more" href="<?php the_permalink(); ?>" title="Read more">Read more &raquo;</a>
		 	<?php } ?>
		 	
		</div>
	</article>
 
<?php endwhile; // End the loop. Whew. ?>
 
<?php if (  $wp_query->max_num_pages > 1 ) :
	if(function_exists('wp_paginate')) {
	    wp_paginate();
	}
endif; ?>