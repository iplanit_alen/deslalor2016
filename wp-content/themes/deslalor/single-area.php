<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */

get_header(); ?>

<section class="block-padding white-bg relative">
	<div class="outer-wrapper cf">

	 		<?php get_template_part( 'loop', 'single' ); ?>

	</div>
</section>

<?php get_footer(); ?>