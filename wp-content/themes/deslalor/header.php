<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
 
    global $page, $paged;
 
    wp_title( '|', true, 'right' );
 
    bloginfo( 'name' );
 
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
        echo " | $site_description";
 
    if ( $paged >= 2 || $page >= 2 )
        echo ' | ' . sprintf( __( 'Page %s', 'starkers' ), max( $paged, $page ) );
 
    ?></title>

<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">    

<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_directory'); ?>/favicon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_directory'); ?>/favicon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_directory'); ?>/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_directory'); ?>/favicon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_directory'); ?>/favicon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_directory'); ?>/favicon/apple-touch-icon-120x120.png">
<link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/favicon/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="<?php bloginfo('template_directory'); ?>/favicon/manifest.json">
<link rel="mask-icon" href="<?php bloginfo('template_directory'); ?>/favicon/safari-pinned-tab.svg" color="#5e9dcb">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">
    
<?php wp_enqueue_script("jquery"); ?>
    
<link rel="profile" href="http://gmpg.org/xfn/11" />

<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_directory'); ?>/css/responsive.css" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/slick.css"/>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/jquery-ui.min.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
 
<script src="<?php bloginfo('template_directory'); ?>/js/modernizr-3.0.js"></script>
 
<?php if ( is_singular() && get_option( 'thread_comments' ) )
        wp_enqueue_script( 'comment-reply' );
 		wp_head();
?>

</head>
 
<body <?php body_class(); ?>>
	
	
<aside id="side-navigation">
	<i id="close-toggle" class="fa fa-close"></i>
    <div class="menu">
		
    </div>
</aside>

	
<div id="allcontent">
	
	<div id="secondary-navigation" class="<?php if(is_front_page()) { ?>navy-bg<?php } else { ?>lightblue-bg<?php } ?>">
		<div class="outer-wrapper">
			<?php wp_nav_menu( array('menu' => 'Home Secondary Menu' )); ?>
		</div>
	</div>
	
	<?php if(get_field('home_slides')): ?>
		<div id="fullheight-slider" class="slickslider">
			<?php while(has_sub_field('home_slides')): ?>
				<?php 
				$image = get_sub_field('home_slide_background_image');
				$size = 'large'; // (thumbnail, medium, large, full or custom size)
				$image_attributes = wp_get_attachment_image_src( $image, $size ); ?>
				<div class="browser-height cover-bg parallax-banner" style="background-image: url(<?php echo $image_attributes[0]; ?>);">
					<img class="parallax-image" src="<?php echo $image_attributes[0]; ?>" alt="" />
					<div class="wrapper">
						<div class="slider-content">
							
							<h4><?php the_sub_field('home_slide_text_content-topline'); ?></h4>
							<h2><?php the_sub_field('home_slide_text_content_-_bottomline'); ?></h2>
							
							<?php $term = get_sub_field('home_slide_button_-_page_link');
							
							if( $term ): ?>
								
								<a class="button" href="<?php echo home_url( '/' ); ?>property-status/<?php echo $term->slug; ?>" title="<?php the_sub_field('home_slide_button_-_text'); ?>"><?php the_sub_field('home_slide_button_-_text'); ?></a>

							<?php endif; ?>
	
						</div>
						<a id="next-section-toggle" href="#home-intro" title="">Scroll</a>
					</div>
				</div>
			<?php endwhile; ?>	
		</div>	
	<?php endif; ?>	
 
	<header id="site-header" class="navy-bg">
		<div class="outer-wrapper cf">
			<a class="logo-link left" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				<img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="" />
			</a>
			<?php wp_nav_menu( array('menu' => 'Home-Primary-Menu' )); ?> 
			<i id="mobile-menu-toggle" class="fa fa-navicon"></i>
		</div>		      
	</header>
	<!-- Site Header End -->
	
	<?php if(!is_front_page()) { ?>
			<header id="page-header" class="lightblue-bg">
				<div class="outer-wrapper cf">
					<div class="breadcrumbs">
						<?php if(function_exists('bcn_display'))
						{
							bcn_display();
						}?>
					</div>
					<h1>
						<?php if(is_home()) { ?>
							<?php if(get_field('custom_blog_title', 'options')) { ?>
								<?php the_field('custom_blog_title', 'options'); ?>
							<?php } else { ?>
								Blog
							<?php } ?>
						<?php } elseif ( is_day() ) { ?>
							<?php printf( __( 'Daily Archives: %s', 'starkers' ), get_the_date() ); ?>
						<?php } elseif ( is_month() ) { ?>
							<?php printf( __( 'Monthly Archives: %s', 'starkers' ), get_the_date('F Y') ); ?>
						<?php } elseif ( is_year() ) { ?>
							<?php printf( __( 'Yearly Archives: %s', 'starkers' ), get_the_date('Y') ); ?>		
						<?php }elseif (is_category()) { ?>
							<?php printf( __( 'Category Archives: %s', 'starkers' ), '' . single_cat_title( '', false ) . '' ); 	?>	
						<?php } elseif ( is_post_type_archive('testimonials') ) { ?>
							Testimonials
						<?php } elseif ( is_tax( 'property-status', 'sold' ) ) { ?>
							Properties we’ve Recently Sold
						<?php } elseif ( is_tax() ) { ?>
							Properties we’re Currently Selling
						<?php } elseif (is_search()) { ?>
							<?php printf( __( 'Search Results for: %s', 'starkers' ), '' . get_search_query() . '' ); ?>
						<?php } else { ?>
							<?php the_title(); ?>
						<?php } ?>
					</h1>
				</div>
			</header>
	<?php } ?>
	
	