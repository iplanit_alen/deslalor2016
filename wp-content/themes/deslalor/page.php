<?php
/**
 * The template for displaying all pages.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */

get_header(); ?>

<section class="block-padding white-bg relative">
	<div class="outer-wrapper cf">

		<?php get_template_part( 'loop', 'page' ); ?>
	
	</div>
</section>


<?php get_footer(); ?>
