<?php
/**
 * The template for displaying the footer.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */
?>

<?php if(!is_tax('property-status')) { ?>	
	<?php 
	$posts = get_field('related_blog_posts');
	if( $posts ): ?>
		<div class="related-posts-wrap white-bg block-padding cf">
			<h2 class="blog-title icon-title">Our Property Market Insights</h2>
			<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
				<?php setup_postdata($post); ?>
			    <article id="post-<?php the_ID(); ?>" <?php post_class('col3 left cf'); ?>>
				    <div class="inner-padding cf">
				    	<h2 class="matchheight"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				    	<div class="matchHeight bottom-margin">
				    		<?php the_excerpt(); ?>
				    	</div>
				    	<?php if(get_field('custom_read_more_text')) { ?>
					 		<a class="read-more right" href="<?php the_permalink(); ?>" title="<?php the_field('custom_read_more_text'); ?>"><?php the_field('custom_read_more_text'); ?> &raquo;</a>
					 	<?php } else { ?>
					 		<a class="read-more right" href="<?php the_permalink(); ?>" title="Read more">Read more &raquo;</a>
					 	<?php } ?>
				    	<span class="post-date"><?php echo get_the_date(); ?></span>		    
				    </div>
			    </article>
			<?php endforeach; ?>
		</div>
		<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	<?php endif; ?>
<?php } ?>


<?php if(get_field('featured_testimonials')) { ?>
	<div id="testimonials-wrapper" class="lightblue-bg white-notch large-padding">
		<div class="wrapper centered">
			<h3>Testimonials</h3>			
			<?php $posts = get_field('featured_testimonials');			 
			if( $posts ): ?>
				<div id="testimonial-slider">
					<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
						<?php setup_postdata($post); ?>
					    <div>
					    	<?php the_content(); ?>
					    	<div class="testimonial-author"><?php the_title(); ?> - <span class="red-highlight"><?php the_field('company'); ?></span></div>
					    </div>
					<?php endforeach; ?>
				</div>
				
				<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>		
			<a class="button" href="<?php echo home_url( '/' ); ?>testimonials/" title="View testimonials">View testimonials</a>	
		</div>
	</div>
<?php } ?>

<?php if(get_field('include_footer_map_block')) { ?>
	<section id="map-block" class="cf">
		<div class="map-wrapper matchheight left">
			<?php 			
			$location = get_field('map_location', 'options');			
			if( !empty($location) ):
			?>
			<div class="acf-map">
				<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
			</div>
			<?php endif; ?>
		</div>
		<?php if(get_field('map_block_text_content', 'options')) { ?>
			<div class="map-content matchheight right lightblue-bg">
				<div class="map-content-inner">
					<?php the_field('map_block_text_content', 'options'); ?>
					<p><strong>Address</strong><br />
					<?php the_field('primary_contact_address', 'options'); ?></p>
					<p><strong>Contact</strong><br />
					<a href="tel:<?php echo str_replace(' ', '', get_field('primary_contact_telephone_number', 'options')); ?>" title="">
							<i class="fa fa-phone"></i> +<?php the_field('primary_contact_telephone_number', 'options'); ?>
						</a>
					<a href="mailto:<?php the_field('primary_contact_email_address', 'options'); ?>" title=""><i class="fa fa-envelope"></i><?php the_field('primary_contact_email_address', 'options'); ?></a></p>
					<?php if(!is_page(10)) { ?>
						<a class="button" href="<?php echo home_url( '/' ); ?>contact" title="">Contact Us</a>
					<?php } ?>
				</div>
			</div>
		<?php } ?>
	</section>
<?php } ?>

<?php if(!is_page(10)) { ?>
<div id="contact-bar" class="med-blue-bg small-padding">
	<div class="outer-wrapper centered">
		<h3><strong>Contact us</strong> so we can help you</h3>
		<a class="button" href="<?php echo home_url( '/' ); ?>contact-us" title="">Contact us</a>
	</div>
</div>
<?php } ?>

<footer id="site-footer" class="navy-bg <?php if(get_field('red_alert_bar_text', 'options')) { ?>alertbar-margin<?php } ?>">
	<div class="wrapper cf">
		<div class="main-footer-menu left">
			<?php wp_nav_menu( array('menu' => 'Footer Menu' )); ?>
		</div>
		<address class="footer-address left">
			<strong>Address</strong>
			<?php the_field('primary_contact_address', 'options'); ?>
		</address>
		<div class="col-half areas-wrapper right">
			<h4><i class="fa fa-search"></i> Areas We Serve</h4>
			<div class="areas">
				<?php wp_nav_menu( array('menu' => 'Areas Menu' )); ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="inner-footer-bar cf">
			<div class="col3 left cf">
				<h3>Newsletter Signup</h3>
				<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true]'); ?>
			</div>
			<div class="col4 right last">
				<a class="logo-link" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="" />
				</a>
			</div>
			<div class="col3 inline-content right">
				<span>Connect with us</span>
				<ul class="social-links">
					<?php if(get_field('twitter_profile_link', 'options')) { ?>
						<li class="twitter"><a href="<?php the_field('twitter_profile_link', 'options'); ?>" target="_blank" title="">
							<i class="fa fa-twitter"></i>
						</a></li>
					<?php } ?>
					<?php if(get_field('facebook_profile_link', 'options')) { ?>
						<li class="facebook"><a href="<?php the_field('facebook_profile_link', 'options'); ?>" target="_blank" title="">
							<i class="fa fa-facebook"></i>
						</a></li>
					<?php } ?>
					<?php if(get_field('youtube_profile_link', 'options')) { ?>
						<li class="youtube"><a href="<?php the_field('youtube_profile_link', 'options'); ?>" target="_blank" title="">
							<i class="fa fa-youtube"></i>
						</a></li>
					<?php } ?>
					<?php if(get_field('linkedin_profile_link', 'options')) { ?>
						<li class="linkedin"><a href="<?php the_field('linkedin_profile_link', 'options'); ?>" target="_blank" title="">
							<i class="fa fa-linkedin"></i>
						</a></li>
					<?php } ?>
					<?php if(get_field('google_plus_profile_link', 'options')) { ?>
						<li class="googleplus"><a href="<?php the_field('google_plus_profile_link', 'options'); ?>" target="_blank" title="">
							<i class="fa fa-google-plus"></i>
						</a></li>
					<?php } ?>
				</ul>			
			</div>
			
		</div>
	</div>
	
	<div class="bottom-bar med-blue-bg small-padding">
		<div class="wrapper cf">
			<div class="footer-logos-wrap left cf">
				<div class="iplanit-logo left">
					<span>Tailored by</span> <a href="http://www.iplanit.ie/" title="" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/iplanit-logo.jpg" alt="" /></a>
				</div>
				<span class="memeber" style="float: left;padding: 5px 12px 0 13px;">MEMBER</span>
				<img class="left" src="<?php bloginfo('template_directory'); ?>/images/footer-logos.jpg" alt="" />
			</div>
			<?php wp_nav_menu( array('menu' => 'Bottom Bar Menu' )); ?>
		</div>
	</div>
	
</footer>

<?php if(get_field('red_alert_bar_text', 'options')) { ?>
	<div id="alertbar" class="alertbar red-bg">
		<div class="outer-wrapper">
			<span class="alert-text"><?php the_field('red_alert_bar_text', 'options'); ?></span>
			<ul class="alert-contact">
				<li><a href="tel:<?php echo str_replace(' ', '', get_field('primary_contact_telephone_number', 'options')); ?>" title=""><i class="fa fa-phone"></i> + <?php the_field('primary_contact_telephone_number', 'options'); ?></a></a></li>
				<li><a href="mailto:<?php the_field('primary_contact_email_address', 'options'); ?>" title=""><i class="fa fa-envelope"></i> <?php the_field('primary_contact_email_address', 'options'); ?></a></li>
			</ul>
		</div>
	</div>
<?php } ?>

</div>
<!-- #allcontent -->

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/slick.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/visible.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.fitvids.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.matchHeight-min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/move.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.acf-map.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.custom.js"></script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56f10c176b37b5e5"></script>

<?php wp_footer(); ?>

</body>
</html>