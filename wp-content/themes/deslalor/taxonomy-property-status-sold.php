<?php
/**
 * Template file for displaying Properties that're Sold.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */
 
get_header(); ?>

<section class="block-padding white-bg relative">
	<div class="outer-wrapper cf">
		<div class="property-wrapper cf">
			<?php 
				$args = array(
				'post_type' => 'our-properties',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'property-status',
						'field'    => 'slug',
						'terms'    => 'sold',
					),
				),
			);


			$the_query = new WP_Query( $args );
				while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<?php if(get_field('property_sold')) { ?>
		 			<article id="post-<?php the_ID(); ?>" <?php post_class('relative col3 left'); ?>>
			 			<figure class="relative">
				    		<div class="property-label">
					    		<span class="left">Sold</span>
				    		</div>
					    	<a href="<?php the_permalink(); ?>" title="">
						    	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'property-thumb' );
						    	$url = $thumb['0']; ?>
						    	<img src="<?php echo $url; ?>" alt="" />
					    	</a>
					    </figure>
				    	<div class="navy-bg property-preview matchheight cf">
					    	<h3><a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a></h3><br />
					    	<span class="subtitle"><?php the_field('property_subtitle'); ?></span>
				    	</div>
				    	<?php if(get_field('sold_listing_-_hover_text')) { ?>
					    	<div class="fadeover overlay navy-bg">
						    	<div class="slide-overlay-inner centered">
							    	<!-- <img class="icon" src="<?php bloginfo('template_directory'); ?>/images/sold-text-icon.jpg" alt="" /> -->
							    	<p><?php the_field('sold_listing_-_hover_text'); ?></p>
							    	<a class="read-more uppercase" href="<?php the_permalink(); ?>" title="">View full story</a>
						    	</div>
					    	</div>
				    	<?php } ?>
					</article>
		 		<?php } ?>	
				<?php endwhile;
			wp_reset_postdata(); ?>

		</div>
		
	 	<header>
	 		<h2 class="sold-title icon-title">Other Sales</h2>
	 	</header>
	 	
	 	<?php 
				$args = array(
				'post_type' => 'our-properties',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'property-status',
						'field'    => 'slug',
						'terms'    => 'sold',
					),
				),
			);

			$the_query = new WP_Query( $args );
	 		while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	 			<?php if(!get_field('property_sold')) { ?>
		 			<article id="post-<?php the_ID(); ?>" <?php post_class('relative col4 left'); ?>>
			 			<figure class="relative">
				    		<div class="property-label">
					    		<span class="left">Sold</span>
				    		</div>
					    	<a href="<?php the_permalink(); ?>" title="">
						    	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'property-thumb' );
						    	$url = $thumb['0']; ?>
						    	<img src="<?php echo $url; ?>" alt="" />
					    	</a>
					    </figure>
				    	<div class="navy-bg property-preview matchheight cf">
					    	<h3><?php the_title(); ?></h3><br />
					    	<span class="subtitle"><?php the_field('property_subtitle'); ?></span>
				    	</div>
				    	<?php if(get_field('sold_listing_-_hover_text')) { ?>
					    	<div class="fadeover overlay navy-bg">
						    	<div class="slide-overlay-inner centered">
							    	<!-- <img class="icon" src="<?php bloginfo('template_directory'); ?>/images/sold-text-icon.jpg" alt="" /> -->
							    	<p><?php the_field('sold_listing_-_hover_text'); ?></p>
							    	<a class="read-more uppercase" href="<?php the_permalink(); ?>" title="">View full story</a>
						    	</div>
					    	</div>
				    	<?php } ?>
					</article>
		 		<?php } ?>	
	 		<?php endwhile;
	 	wp_reset_postdata(); ?>

		 	
	</div>
</section>

<?php get_footer(); ?>