<?php
/**
 * Template Name: Contact page
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */

get_header(); ?>

<section id="contact-page" class="white-bg block-padding">
	<div class="outer-wrapper cf">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class('contact-form-wrap left cf'); ?>>			
				<?php the_content(); ?>
			</article>
			
			<aside class="width-14 right">
				<h3>Contact details</h3>
				<ul class="contact-details">
					<li>
						<a href="mailto:<?php the_field('primary_contact_email_address', 'options'); ?>" title="">
							<i class="fa fa-envelope"></i> <?php the_field('primary_contact_email_address', 'options'); ?>
						</a>
					</li>
					<li>
						<a href="tel:<?php echo str_replace(' ', '', get_field('primary_contact_telephone_number', 'options')); ?>" title="">
							<i class="fa fa-phone"></i> + <?php the_field('primary_contact_telephone_number', 'options'); ?>
						</a>
					</li>
					<li>
						<i class="fa fa-home"></i> <?php the_field('primary_contact_address', 'options'); ?>
					</li>
				</ul>
				<?php the_field('content_below_contact_details'); ?>
			</aside>
		
		<?php endwhile; ?>
	</div>
</section>

<?php get_footer(); ?>