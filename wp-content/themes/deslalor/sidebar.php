<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */
?>

<aside id="blog-sidebar" class="site-sidebar width-14 right">
	<ul>
		<li>
			<form method="get" id="searchform" action="<?php bloginfo('home'); ?>/">
				<div>
					<input type="text" name="s" id="s" value="Search by Keyword" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;"/>
					<input type="submit" id="searchsubmit" value="Search" class="btn" />
				</div>
			</form>
			<!-- Search Form End -->
		</li>
		<?php dynamic_sidebar( 'primary-widget-area' ); ?>
	</ul>	
</aside>